########################################################################################################################
# processDataset.py
# Description: Loads the dataset for the Titanic: Machine Learning from Disaster (https://www.kaggle.com/c/titanic)
# challenge and creates the appropriate train, validation test splits.
# Author: Adhithyan Ramadoss - ramadoss.adhithyan@gmail.com
# Created on: 21.07.2020
########################################################################################################################
import pandas as pd
from sklearn.model_selection import train_test_split


class LoadData:
    """
    Class to load data from csv files and return the train, validation, test sets.
    input:
    directory_path - Path to folder containing the dataset.
    validation_split - Amount of train data to be used for validation (default: 0.1).
    output:
    train_data - training data split
    validation_data - validation data split
    test_data - test data split
    """
    def __init__(self, directory_path, validation_split=0.1):
        self.directory_path = directory_path
        self.validation_split = validation_split

    def load_dataset(self):
        if self.validation_split == 0:
            train_data = pd.read_csv(self.directory_path + "train.csv")
            validation_data = pd.DataFrame()
        else:
            train_data, validation_data = train_test_split(pd.read_csv(self.directory_path + "train.csv"),
                                                           test_size=self.validation_split)
        test_data = pd.read_csv(self.directory_path + "test.csv")
        return train_data, validation_data, test_data
