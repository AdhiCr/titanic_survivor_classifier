This is our take on the Titanic: Machine Learning from Disaster (https://www.kaggle.com/c/titanic) prediction competition, targeted at entry level ML aspirants.
The authours in thsi project are Adhithyan Ramadoss (AR) and Pravin Kumar Marimuthu (PM).

The files in the project are structured as:

```
Titanic_survivor_classifier
|_ __init__.py
|_ main.py
|_ processDataset.py (AR) 
|_ <classifierMethod1.py> (AR)
|_ <classifierMethod2.py>
|_ <classifierMethod3.py>
|_ <classifierMethod4.py>
|_ plotResults.py (AR)
|_ dataset
    |_ gender_submission.csv
    |_ notes.txt
    |_ test.csv
    |_ train.csv
```