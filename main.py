########################################################################################################################
# main.py
# Description: Loads all the required script files in a sequential order and performs the ensembling of different
# classifiers' results
# Author: Adhithyan Ramadoss - ramadoss.adhithyan@gmail.com,
# Created on: 21.07.2020
########################################################################################################################
from processDataset import LoadData


classifier_list = ["SVM", "Random_Forest"]
ensemble = True


if "SVM" in classifier_list:
    print("Loading data for SVM...")
    data_split = LoadData(directory_path="./dataset/", validation_split=0)
    train_data, _, test_data = data_split.load_dataset()
    #train_data.head(1).info()  # To check the data in the dataframe. Comment during execution
    #test_data.head(1).info()  # To check the data in the dataframe. Comment during execution
    temp = train_data.isnull().sum()
    temp_2 = train_data.isnull().count()
    print("Here")


if ensemble:
    pass

